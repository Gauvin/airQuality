# Introduction

This is a collection of R files that reads an XML air quality feed from the city of Montreal at different dates :

`http://ville.montreal.qc.ca/rsqa/servlet/makeXmlActuel?date={.x}`

The xml file contains hourly measurements at different stations for a fixed date.

IQA: indice de qualite de l'air

```
     head
     |
     |-- journee
         |
         | -- station (id: int, donnes: bool)
              |-- echantillon (heure)
                   |-- qualite
                   |-- polluant (nom: string( CO, NO2, O3, PM, SO2), value)
```



---



# Command line usage

## With start end end dates

`Rscript main.R --startDate '2020-01-01' --endDate '2022-01-01'`

## With start date and number of days

`Rscript main.R --startDate '2020-01-01' --numDays 100`

These will read off an XML feed and write results to `airQuality/Data/RData/startDate_EndDate.Rdata`



---



# Other uses

## Causal impact of the pandemic on air quality

The scripts in `Rmarkdown` read in the saved .Rdata files if they exist for fixed start end end dates.

`causalImpact` uses the `CausalImpact` package from Google and tries to identify causal effects by using a simple time series and assuming the trends should remain constant given no treatment (basically the overall air quality trend is independent of the pandemic.)

Assess the causal impact of the 2020 pandemic lockdown on the air quality in Montreal using the open access data measurements on 4 air quality indicators in 4 regions. See <https://www.donneesquebec.ca/recherche/fr/dataset/vmtl-rsqa-indice-qualite-air#avertissementTelechargement_c8d165f4_c3c8_43de_aaa0_e9adc4ada2de>.
